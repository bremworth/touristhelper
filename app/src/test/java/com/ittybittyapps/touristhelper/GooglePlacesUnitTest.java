package com.ittybittyapps.touristhelper;


import com.google.android.gms.maps.model.LatLng;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ittybittyapps.touristhelper.googleplaces.GooglePlaces;
import com.ittybittyapps.touristhelper.googleplaces.GooglePlacesAsyncRequest;
import com.ittybittyapps.touristhelper.model.PlaceResources;

import junit.framework.Assert;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class GooglePlacesUnitTest {
    @Test
    public void test_google_places_urlbuilder() throws Exception {
        GooglePlaces googlePlaces = new GooglePlaces();

        LatLng latLng = new LatLng(-37.801076, 144.977993);
        String requestURL = googlePlaces.requestBuilder(latLng);
        System.out.println(requestURL);
        Assert.assertEquals(requestURL, "mystringhere");
    }

    @Test
    public void test_googlePlacesAPI_json_parsing() throws Exception {
        PlaceResources placeResources;
        Gson gson = new GsonBuilder().serializeNulls().create();
        placeResources = gson.fromJson(loadJSONFromFile("testdata.json"), PlaceResources.class);

        Assert.assertEquals(placeResources.getResults().get(0).getName(), "Stuzzi");
        Assert.assertEquals(placeResources.getResults().get(0).getGeometry().getLocation().getLat(), -37.7709004);
        Assert.assertEquals(placeResources.getResults().get(0).getGeometry().getLocation().getLng(), 144.9983924);

    }


    private String loadJSONFromFile(String f) {
        String json;
        try {

            File file = new File(f);
            FileInputStream fileInputStream = new FileInputStream(file);

            int size = fileInputStream.available();

            byte[] buffer = new byte[size];

            fileInputStream.read(buffer);

            fileInputStream.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;


    }
}