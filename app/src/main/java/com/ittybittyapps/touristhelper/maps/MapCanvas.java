package com.ittybittyapps.touristhelper.maps;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.ittybittyapps.touristhelper.app.activity.MapsActivity;

import java.util.List;


/**
 * Created by alex on 5/04/17.
 */

public class MapCanvas {

    /**
     *
     * @param locations - and order list of latlng points to draw a connected path
     */
    public void drawContentedLines(List<LatLng> locations) {
        //We draw lines connecting from
        for(int i = 0; i < locations.size()-1; ++i) {
            Polyline line = MapsActivity.getMap().addPolyline(new PolylineOptions()
                    .add(locations.get(i), locations.get(i+1))
                    .width(5)
                    .color(Color.RED));
        }
        //Finish the path back to the start
        Polyline line = MapsActivity.getMap().addPolyline(new PolylineOptions()
                .add(locations.get(locations.size()-1), locations.get(0))
                .width(5)
                .color(Color.RED));
    }
}
