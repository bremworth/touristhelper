package com.ittybittyapps.touristhelper.model.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by alex on 13/04/17.
 */

public class Location  {

    @SerializedName("lat")
    private Double lat;

    @SerializedName("lng")
    private Double lng;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
