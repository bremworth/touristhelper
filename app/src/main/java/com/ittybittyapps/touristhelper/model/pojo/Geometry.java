package com.ittybittyapps.touristhelper.model.pojo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by alex on 13/04/17.
 */

public class Geometry  {

    @SerializedName("location")
    private Location location;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
