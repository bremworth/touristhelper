package com.ittybittyapps.touristhelper.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.ittybittyapps.touristhelper.model.pojo.Place;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 13/04/17.
 */

public class PlaceResources {

    private static List<Place> resultSets = new ArrayList<>();

    @SerializedName("debug_info")
    private List<String> debug_info;

    @SerializedName("html_attributions")
    private List<String> html_attributions;

    @SerializedName("next_page_token")
    private String next_page_token;

    @SerializedName("results")
    private List<Place> results;

    public List<Place> getResultSets() {
        return resultSets;
    }

    public void addResultSet(PlaceResources set) {
        resultSets.addAll(set.getResults());
    }

    public void add(Place place) {
        resultSets.add(place);
    }

    public List<LatLng> fetchLocations() {
        List<LatLng> locations = new ArrayList<>();
        for(Place place : resultSets) {
            locations.add(new LatLng(place.getGeometry().getLocation().getLat(),place.getGeometry().getLocation().getLng()));
        }
        return locations;
    }

    public List<String> getDebug_info() {
        return debug_info;
    }

    public void setDebug_info(List<String> debug_info) {
        this.debug_info = debug_info;
    }

    public List<String> getHtml_attributions() {
        return html_attributions;
    }

    public void setHtml_attributions(List<String> html_attributions) {
        this.html_attributions = html_attributions;
    }

    public String getNext_page_token() {
        return next_page_token;
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    public List<Place> getResults() {
        return results;
    }

    public void setResults(List<Place> results) {
        this.results = results;
    }
}
