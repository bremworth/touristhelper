package com.ittybittyapps.touristhelper.model.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 18/04/17.
 */

public class OpeningHours {
    @SerializedName("open_now")
    private Boolean openNow;
    @SerializedName("weekday_text")
    private List<Object> weekdayText = new ArrayList<Object>();

    public List<Object> getWeekdayText() {
        return weekdayText;
    }

    public void setWeekdayText(List<Object> weekdayText) {
        this.weekdayText = weekdayText;
    }

    public Boolean getOpenNow() {
        return openNow;
    }

    public void setOpenNow(Boolean openNow) {
        this.openNow = openNow;
    }
}
