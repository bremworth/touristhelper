package com.ittybittyapps.touristhelper.graph;

import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * Created by alex on 5/04/17.
 */
public class Graph {

    private int number_of_places;
    private Stack<Integer> stack;

    public Graph() {
        stack = new Stack<>();
    }

    public List<LatLng> travelingTourist(List<LatLng> unordered) {
        int adjacencyMatrix[][] = buildGraph(unordered);
        Log.d("Graph", "size: " + unordered.size());

        List<Integer> waypoints =  tsp(adjacencyMatrix);
        List<LatLng> ordered = new ArrayList<>();
        for(int waypoint : waypoints) {
            ordered.add(unordered.get(waypoint));
        }
        Log.d("Graph", "Ordered: "+ordered.toString());
        return ordered;
    }

    public int[][] buildGraph(List<LatLng> places) {
        number_of_places = places.size();

        int adjacency_matrix[][] = new int[number_of_places][number_of_places];
        for (int i = 0; i < number_of_places; i++) {
            LatLng a = places.get(i);
            for (int j = 0; j < number_of_places; j++) {
                LatLng b = places.get(j);

                adjacency_matrix[i][j] = (int) SphericalUtil.computeDistanceBetween(a,b);
            }
        }

        return adjacency_matrix;
    }

    /**
     * Traveling Salesman Problem using Nearest Neighbour with the input
     * being a Adjacency Matrix of distances from each location
     */
    private List<Integer> tsp(int adjacencyMatrix[][]) {

        number_of_places = adjacencyMatrix[1].length - 1;
        List<Integer> waypoints = new ArrayList<>();
        int[] visited = new int[number_of_places + 1];

        visited[1] = 1;
        stack.push(1);

        int destination = 0;
        boolean minFlag = false;

        waypoints.add(0);

        while (!stack.isEmpty()) {
            int element = stack.peek();
            int i = 1;
            int min = Integer.MAX_VALUE;
            while (i <= number_of_places) {
                if (adjacencyMatrix[element][i] > 1 && visited[i] == 0) {
                    if (min > adjacencyMatrix[element][i]) {
                        min = adjacencyMatrix[element][i];
                        destination = i;
                        minFlag = true;
                    }
                }
                i++;
            }
            if (minFlag) {
                visited[destination] = 1;
                stack.push(destination);
                waypoints.add(destination);
                minFlag = false;
                continue;
            }
            stack.pop();
        }
        return waypoints;
    }
}
