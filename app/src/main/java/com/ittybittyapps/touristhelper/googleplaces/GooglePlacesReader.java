package com.ittybittyapps.touristhelper.googleplaces;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ittybittyapps.touristhelper.app.activity.MapsActivity;
import com.ittybittyapps.touristhelper.model.pojo.Place;
import com.ittybittyapps.touristhelper.model.PlaceResources;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 13/04/17.
 */
public class GooglePlacesReader {
    private GooglePlaces googlePlaces;

    public GooglePlacesReader() {
        googlePlaces = new GooglePlaces();
    }


    public void parseResults(String result) {
        GooglePlacesAsyncReader googlePlacesAsyncReader = new GooglePlacesAsyncReader();
        // Start parsing the Google places in JSON format
        // Invokes the "doInBackground()" method of the class GooglePlacesAsyncReader
        googlePlacesAsyncReader.execute(result);
    }


    private class GooglePlacesAsyncReader extends AsyncTask<String, Integer, PlaceResources> {
        // Invoked by execute() method of this object
        @Override
        protected PlaceResources doInBackground(String... jsonData) {
            PlaceResources placeResources = null;
            try {
                Gson gson = new GsonBuilder().serializeNulls().create();
                placeResources = gson.fromJson(jsonData[0], PlaceResources.class);
                if(placeResources == null)
                    throw new RuntimeException("Places Results is null");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return placeResources;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(PlaceResources results) {
            Log.d("Map", "list size: " + results.getResults().size());
            //Build map markers from the given list of places
            results.addResultSet(results);
            if(results.getNext_page_token() != null) {
                requestNextPage(results.getNext_page_token());
            } else {
                buildMarkers(results.getResultSets());
                googlePlaces.buildTouristTrail(results.fetchLocations()); //TODO
            }
        }
    }

    /**
     * When the JSON is returned there will be a next_page_token that can be used to retrieve
     * the next page of results for the current search.
     * If there are no page tokens then we want to build the tourist trail from the current
     * set of results.
     * We have to do multiple page requests for more places which require a waiting time
     * between requests
     */
    private void requestNextPage(String nextpagetoken) {
        final String nextpage = googlePlaces.requestBuilder(nextpagetoken);
        //Running a new API request within the async task
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after set time
                //query places with current location
                GooglePlacesAsyncRequest googlePlacesAsyncRequest = new GooglePlacesAsyncRequest();
                googlePlacesAsyncRequest.execute(nextpage);
            }
        }, 3000); //3 seconds
    }


    private static int marker_count = 0;
    private static final int MAX_COUNT = 100;
    /**
     * Builds markers on the map from a list of places
     * @param list - The places response
     * @return a list of LatLng points
     */
    private List<LatLng> buildMarkers(List<Place> list) {

        List<LatLng> locations = new ArrayList<>();
        //Skip myLocation
        for (int i = 1; i < list.size(); i++) {
            Place place = list.get(i);
            //Since we are running multiple searches to build up the places of interest list
            //we want to keep on the total marker count and just complete the task when we hit the limit
            if (marker_count > MAX_COUNT)
                return locations;
            marker_count++;

            // Getting a place from the places list

            double lat = place.getGeometry().getLocation().getLat();
            double lng = place.getGeometry().getLocation().getLng();

            LatLng latLng = new LatLng(lat, lng);

            locations.add(latLng);

            // Getting name
            String name = place.getName();
            String vicinity = place.getVicinity();

            String type = place.getTypes().get(0);

            Log.d("Map", "place: " + name);

            // Creating a marker
            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(name);
            markerOptions.snippet(type + ":" + vicinity);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

            // Placing a marker on the touched position
            Marker m = MapsActivity.getMap().addMarker(markerOptions);
        }

        return locations;
    }

}
