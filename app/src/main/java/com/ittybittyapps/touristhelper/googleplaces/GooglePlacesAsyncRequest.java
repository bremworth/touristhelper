package com.ittybittyapps.touristhelper.googleplaces;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by alex on 13/04/17.
 */
public class GooglePlacesAsyncRequest extends AsyncTask<String, Integer, String> {

    // Invoked by execute() method of this object
    @Override
    protected String doInBackground(String... url) {
        String data = null;
        try {
            data = requestPlaces(url[0]);
        } catch (Exception e) {
            Log.d("Background Task", e.toString());
        }
        return data;
    }

    // Executed after the complete execution of doInBackground() method
    @Override
    protected void onPostExecute(String result) {
        Log.d("result", "<><> result: " + result);
        GooglePlacesReader googlePlacesReader = new GooglePlacesReader();
        googlePlacesReader.parseResults(result);

    }


    /**
     * Http request to the Google Places API
     *
     * @param strUrl - The http Google Places API request URL
     * @return The raw response json as a string
     */
    private String requestPlaces(String strUrl) {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            Log.d("Places", "RequestURL: " + strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuilder sb = new StringBuilder();

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            try {
                iStream.close();
                urlConnection.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return data;

    }
}
