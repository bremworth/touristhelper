package com.ittybittyapps.touristhelper.googleplaces;

import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.ittybittyapps.touristhelper.graph.Graph;
import com.ittybittyapps.touristhelper.maps.MapCanvas;
import java.util.List;

/**
 * Created by alex on 5/04/17.
 *
 */
public class GooglePlaces {

    //TODO allow for multiple types
    private static final String type = "point_of_interest";

    private static final String API_KEY = "";

    public void runGooglePlacesAPISearch(String requestURL) {
        //query places with current location
        GooglePlacesAsyncRequest googlePlacesAsyncRequest = new GooglePlacesAsyncRequest();
        googlePlacesAsyncRequest.execute(requestURL);
    }

    void buildTouristTrail(List<LatLng> unordered) {

        Graph graph = new Graph();
        MapCanvas mapCanvas = new MapCanvas();

        List<LatLng> ordered = graph.travelingTourist(unordered);
        mapCanvas.drawContentedLines(ordered);
    }

    /**
     Defines the distance (in meters) within which to bias place results.
     The maximum allowed radius is 50 000 meters. Results inside of this region
     will be ranked higher than results outside of the search circle; however, prominent
     results from outside of the search radius may be included.
     */
    private static final int SEARCH_RADIUS = 2000;
    private static final String AUTHORITY = "maps.googleapis.com";
    private static final String ABSOLUTE_PATH = "/maps/api/place/nearbysearch/json";

    /**
     * Google Places API - https://developers.google.com/places/web-service/search
     * @param pagetoken - request the next page of places from the search via the given token
     * @return returns the Http request string for the Google Places API
     */
    String requestBuilder(String pagetoken) {
        Uri.Builder requestURL = new Uri.Builder();
        requestURL.scheme("https")
                .authority(AUTHORITY)
                .path(ABSOLUTE_PATH)
                .appendQueryParameter("key", API_KEY)
                .appendQueryParameter("pagetoken", pagetoken).build();

        return requestURL.toString();
    }

    /**
     * Google Places API - https://developers.google.com/places/web-service/search
     * @param location - the initial search location, usually the users MyLocation
     * @return returns the Http request string for the Google Places API
     */
    public String requestBuilder(LatLng location) {
        Uri.Builder requestURL = new Uri.Builder();
        requestURL.scheme("https")
                .authority(AUTHORITY)
                .path(ABSOLUTE_PATH)
                .appendQueryParameter("location", location.latitude + "," + location.longitude)
                .appendQueryParameter("radius", Integer.toString(SEARCH_RADIUS))
                .appendQueryParameter("type", type)
                .appendQueryParameter("key", API_KEY).build();

        return requestURL.toString();
    }


}
