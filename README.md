# README #

### What is this repository for? ###

* Developed for Itty Bitty Apps as a demo
* An app that will help tourists find sights to see wherever they may be. 
Fetches about 100 interesting locations around the deviceâ€™s current location. Each location is plotted on
a map view with information about each location when selected.
A route is drawn that connects all the locations together and ends back at the current location in the shortest path possible
The paths are as the crow flies.

### How do I get set up? ###

* A Google Maps API key and Google Places API key are required to run the application which can be obtain from
    setting up a Google API account here:     https://developers.google.com/maps/documentation/android/start#get-key

* Clone and build through Android Studio